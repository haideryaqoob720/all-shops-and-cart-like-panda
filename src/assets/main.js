import $ from 'jquery'

$(document).ready(function () {
  var scrollLink = $('.scroll')

  $(window).scroll(function () {
    var scrollbarLocation = $(this).scrollTop()
    scrollLink.each(function () {
      var sectionOffset = $(this.hash).offset().top - 350
      if (sectionOffset <= scrollbarLocation) {
        $(this).parent().addClass('active')
        $(this).parent().siblings().removeClass('active')
      }
    })
  })
})

// $(document).ready(function () {
//   var scrollLink = $('.scroll')

//   $(window).scroll(function () {
//     var scrollbarLocation = $(this).scrollTop()
//     scrollLink.each(function () {
//       var sectionOffset = $(this.hash).offset().top - 350
//       if (sectionOffset <= scrollbarLocation) {
//         if ($(this).parent().addClass('active')) {
//           $(document).ready(function () {
//             function myFunction () {
//               var elmnt = document.getElementById('hello')
//               elmnt.scrollLeft += 1
//             }
//             myFunction()
//           })
//         }
//         $(this).parent().parent().css({ transform: 'translateX(0px)' })
//         $(this).parent().siblings().removeClass('active')
//       }
//     })
//   })
// })
